#!/bin/sh
HOST=$1
echo "Waiting for postgres..."

while ! nc -z $HOST 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

gunicorn -b 0.0.0.0:5000 manage:app