

### Common Commands

Build the images:
```
$ docker-compose  build
```
Run the containers:
```
$ docker-compose  up -d
```
Create the database:
```
$ docker-compose run devopsx-services python manage.py recreate_db
```

Seed the database:
```
$ docker-compose run devopsx-services python manage.py seed_db
```
Run the tests:
```
$ docker-compose run devopsx-services python manage.py test
```

Run the tests with coverage:
```
$ docker-compose run devopsx-services python manage.py cov
```

Run checking stylistic and programming errors:
```
$ docker-compose run devopsx-services flake8 project
```
